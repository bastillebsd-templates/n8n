## Status
[![pipeline status](https://gitlab.com/bastillebsd-templates/n8n/badges/master/pipeline.svg)](https://gitlab.com/bastillebsd-templates/n8n/commits/master)


## n8n
Bastille Template for a n8n

## Bootstrap

```shell
ishmael ~ # bastille bootstrap https://gitlab.com/bastillebsd-templates/n8n
```

## Usage

```shell
ishmael ~ # bastille template TARGET bastillebsd-templates/n8n
```
